Docker image for testing and depoying Django web app.

This image contains these softwares:

- Python 3
- PostgreSQL
- Redis
- Ansible (for deploying)
- Some lib for building Python modules

This image doesn't contain Django. Django and other Python modules should be installed separately in container.

